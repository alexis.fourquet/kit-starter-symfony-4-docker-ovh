Forked from https://github.com/romaricp/kit-starter-symfony-4-docker

Kit starter for a Symfony 4 project with Docker and php 7.3. 
This adds php extensions to be closer to [OVH shared hosting](http://fpm7.3-check.cluster015.ovh.net/phpinfo.php)
\+ inspired by [LP 111](https://boutique.ed-diamond.com/numeros-deja-parus/1368-linux-pratique-111.html)

### Containers :
- sf4_apache (http + https)
- sf4_php (Php 7.3.2 + fpm)
- sf4_phpmyadmin (on port 8080)
- sf4_mysql
- sf4_maildev ([maildev](https://github.com/djfarrelly/MailDev) to test emails, on port 8001)

### Build

```bash
docker-compose up --build
# or docker-compose build
```

### SSL

Generate Root CA and a self signed ssl certificate to have https support [-> doc](https://gist.github.com/fntlnz/cf14feb5a46b2eda428e000157447309)

```bash
cd .docker/config/vhosts/certs
# Create Root Key
openssl genrsa -out ./rootCA.key 4096
# Create and self sign the Root Certificate
openssl req -x509 -new -nodes -key ./rootCA.key -sha256 -days 366 -out ./rootCA.crt
# then import the file rootCA.crt in your web browser to avoid warnings

#Create the certificate key
openssl genrsa -out ./ssl/private/apache-selfsigned.key 2048
#Create the signing (csr)
openssl req -new -sha256 -key ./ssl/private/apache-selfsigned.key -subj "/C=FR/ST=State/O=TestDev, Inc./CN=127.0.0.1" -out ./testdev.csr
#Verify the csr's content
openssl req -in ./testdev.csr -noout -text
#Generate the certificate using the testdev csr and key along with the CA Root key
openssl x509 -req -in ./testdev.csr -CA ./rootCA.crt -CAkey ./rootCA.key -CAcreateserial -out ./ssl/certs/apache-selfsigned.crt -days 365 -sha256
#Verify the certificate's content
openssl x509 -in ./ssl/certs/apache-selfsigned.crt -text -noout
```

### Connect to containers
```bash
docker exec -it sf4_apache bash
```
```bash
docker exec -it sf4_php bash
# then run composer, phpunit, node, etc...
```
```bash
docker exec -it sf4_phpmyadmin sh
```
```bash
docker exec -it sf4_mysql bash
```
```bash
docker exec -it sf4_maildev sh
```
